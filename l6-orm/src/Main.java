import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Main {

    final static String CREATE = "CREATE TABLE sometable(id SERIAL, name VARCHAR(20) ) ";

    public static void main(String[] args) {

        try {
            Class.forName("org.postgresql.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:54320/crm?user=dev&password=dev");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Statement st = null;
        try {
            st = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            st.execute(CREATE);
        } catch (SQLException e) {
            e.printStackTrace();
        }

/*
        Finterface lala = new Finterface() {
            @Override
            public String doSomething() {
                return null;
            }
        };

        Finterface la = () -> "text";
        String result = someMethod(la);

        System.out.println(result);

        Thread mt = new Thread(new MyThread());
        mt.start();

        //////
        new Thread(() -> System.out.println("text2")).start();
        //////

        System.out.println(Thread.activeCount());

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("/home/ivan/work/java-courses/l6-orm/config.properties"));
            System.out.println(prop.getProperty("something"));
        } catch (IOException e) {
            e.printStackTrace();
        }
*/



    }

    private static String someMethod(Finterface me) {
        return me.doSomething() + " is a text";

    }
}
