import java.io.File;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {

        List<String> params = Arrays.asList(args);

        String dir = "";
        for (String param : params) {
            if (param.startsWith("--dir=")) {
                dir = param.substring("--dir=".length());
            }
        }

        ensure(dir.length() > 0, "Invalid directory");

        File folder = new File(dir);
        List<File> dirs = new ArrayList<>();
        List<File> files = new ArrayList<>();

        System.out.println("Files in directory \"" + dir + "\":");
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                dirs.add(file);
            } else {
                files.add(file);
            }
        }

        Collections.sort(dirs);
        System.out.print(ConsoleColors.BOLD);
        System.out.print(ConsoleColors.ITALIC);
        System.out.print(ConsoleColors.UNDERLINE);
        System.out.print(ConsoleColors.YELLOW_BOLD_UNDERLINED);
        for (File dirr : dirs) {
             System.out.println(dirr.getName());
        }
        System.out.print(ConsoleColors.RESET);

        Collections.sort(files);
        System.out.print(ConsoleColors.CYAN_BRIGHT);
        for (File file : files) {
            System.out.println(file.getName());
        }
        System.out.print(ConsoleColors.RESET);


    }

    private static void ensure(boolean statement, String errorText) throws Exception {
        if (!statement) {
            throw new Exception(errorText);
        }
    }
}
