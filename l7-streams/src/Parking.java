import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parking {
    public static void main(String[] args) {

        Map<Integer, Map<Integer, Car>> parking = generateParking();

        List<Car> Cars = new ArrayList<Car>();


        Map<Integer, Map<Integer, Car>> result = parking.entrySet()
                .stream()
                .filter(floor -> floor
                        .getValue()
                        .values()
                        .stream()
                        //.filter(car -> true)
                        .filter(car -> car != null)
                        .count() >= 2
                        )
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue()
                        )
                );

        List<Car> cars = new ArrayList<Car>();
        result.values().forEach(m -> cars.addAll(m.values()));
        System.out.println(cars);

        /*
        List<Car> cars = result
                .entrySet()
                .stream()
                .map(entry -> entry
                        .getValue()
                        .values()
//                        .stream()
//                        .collect(Collectors.toList())
                )
                .collect(Collectors.toList())
                ;
        */
        displayParking(result);
    }

    private static Map<Integer, Map<Integer,Car>> generateParking() {

        Map<Integer, Car> floor1 = new TreeMap<Integer, Car>();
        floor1.put(1, new Car().setMark("Opel").setYear(2000));
        floor1.put(2, new Car().setMark("Mercedes").setYear(2020));
        floor1.put(3, null);

        Map<Integer, Car> floor2 = new TreeMap<Integer, Car>();
        floor2.put(1, null);
        floor2.put(2, new Car().setMark("Shkoda").setYear(1990));
        floor2.put(3, null);

        Map<Integer, Map<Integer, Car>> parking = new TreeMap<Integer, Map<Integer, Car>>();

        parking.put(1, floor1);
        parking.put(2, floor2);

        return parking;
    }

    private static void displayParking(Map<Integer, Map<Integer, Car>> parking){

        for(Integer floorNumber : parking.keySet()){
            System.out.println("Floor: " + floorNumber);
            Map<Integer, Car> floor = parking.get(floorNumber);
            for (Integer place : floor.keySet()) {
                Car car = floor.get(place);
                System.out.println(place + ": " + car);
            }
        }
        /*
        for(Map.Entry<Integer, Map<Integer, Car>> entry: parking.entrySet()) {
            System.out.println("Floor: " + entry.getKey());
            for (Map.Entry<Integer, Car> floor : entry.getValue().entrySet()) {
                System.out.println(floor.getKey() + ": " + floor.getValue());
            }
        }
        */
    }
}
