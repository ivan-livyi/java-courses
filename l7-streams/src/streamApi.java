import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class streamApi {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("a", "b", "c");
        stream.forEach(s -> System.out.println(s + " "));

        List<String> list = new ArrayList<String>(Arrays.asList("d", "e", "f"));

        stream = list.stream();
        //stream.distinct().forEach(s -> System.out.println(s + " "));

        long l = stream.distinct().count();
/*
        stream.filter(s -> s.contains("d"));

        list.stream()
                .filter(s -> s.contains("d"))
                .collect(Collectors.toList());
*/

        Map<String, Integer> numb = new HashMap<String, Integer>();

        numb.put("one", 1);
        numb.put("two", 2);
        numb.put("three", 3);

        List<Number> numblist = numb.entrySet()
                .stream()
                .map(e -> new Number().str(e.getKey()).integer(e.getValue()))
                .filter(n -> n.getValue() > 1)
                .collect(Collectors.toList());

        Optional<Number> number = numblist.stream().findAny();
        if (number.isEmpty()) {

        }
        number.ifPresent(n -> System.out.println(n));

        System.out.println(number.get().getValue());

        List<Description> d = new ArrayList<Description>();
        d.add(new Description(Arrays.asList("d1", "d2")));

        Stream<String> strings = d.stream()
                .flatMap(desc -> desc.getDescs().stream());

        strings.forEach(s -> System.out.println(s + " "));
    }
}