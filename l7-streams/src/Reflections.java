import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Reflections {
    public static boolean isSerializable(Object obj) {
        boolean serializable = false;

        Class<? extends Object> clazz = obj.getClass();

        if (clazz.getAnnotation(JSONSerializable.class) != null) {
            serializable = true;
        }

        return serializable;
    }

    public static String getJson(Object obj) throws IllegalAccessException {

        Class<? extends Object> clazz = obj.getClass();
        Map<String, Object> map = new HashMap<String, Object>();

        for (Field field : clazz.getDeclaredFields()) {
            JSONElement elAnn = field.getAnnotation(JSONElement.class);

            if (field.getAnnotation(JSONElement.class) != null) {
                field.setAccessible(true);

                map.put(elAnn.key().isEmpty() ? field.getName() : elAnn.key(), field.get(obj));
            }
        }
        String json = map.entrySet().stream()
                .map(entry -> "\"" + entry.getKey() + "\" : \"" + entry.getValue() + "\"")
                .collect(Collectors.joining(","));

        return "{" + json + "}";
    }
/*
    public static void methodInvocating(Object obj) throws InvocationTargetException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getAnnotation(MethodInv.class) != null) {
                method.setAccessible(true);
                method.invoke(obj);
            }
        }
    }
*/
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Dog dog = new Dog();
        dog.setAge(10);
        dog.setName("Doggo");
        String json = "";

        if (isSerializable(dog)) {
            try {
                json = getJson(dog);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        System.out.println(json);
        //methodInvocating(dog);
    }
}
