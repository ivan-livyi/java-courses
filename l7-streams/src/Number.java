public class Number {
    private String str;

    private Integer value;

    public Number str(String str) {
        this.str = str;
        return this;
    }

    public Number integer(Integer integer) {
        this.value = integer;
        return this;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public Integer getValue() {
        return value;
    }
}
