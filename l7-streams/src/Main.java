import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;
import java.util.function.*;

public class Main {

    public static Integer toInt(Function<String, Integer> f, String s) {
        return f.apply(s);
    }

    public static Integer toInt2(BiFunction<String, Boolean, Integer> f, String s, boolean b) {
        return f.apply(s, b);
    }

    public static void main(String[] args) {
/*        Function<String, Integer> func = (s) -> Integer.parseInt(s);

        int i = toInt(func, "123");
        System.out.println((i);

        BiFunction<String, Boolean, Integer> bif = (s, b) -> b ? Integer.parseInt(s)
                : new Integer(-1);

        System.out.println(toInt2(bif, "123", false));

        Consumer<String> cons = (s) -> System.out.println(s.toUpperCase());

        sout(cons, "someString");

        BiConsumer<String, Integer> biCons = (s, in) -> System.out.println(s + " : " + i);
        sout2(biCons, "s", 123);

        Map<String, Integer> salarias = new HashMap<String, Integer>();
        salarias.put("name", 1000);
        salarias.put("name2", 2000);
        salarias.put("name3", 3000);

        salarias.forEach((name, salary) ->
                salarias.put(name, salary + 1000)
        );

        System.out.println(salarias);


        Supplier<Double> rnd = () -> Math.random();

        System.out.println(rnd.get());*/
    }


    public static void sout2(BiConsumer<String, Integer> cons, String a, Integer b) {
        cons.accept(a, b);
    }

    public static void sout(Consumer<String> cons, String s) {
        cons.accept(s);
    }
}
