import java.util.Arrays;
import java.util.List;

public class Description {
    private List<String> descr;

    public Description(List<String> descr) {
        this.descr = descr;
    }

    public List getDescs() {
        return this.descr;
    }
}
