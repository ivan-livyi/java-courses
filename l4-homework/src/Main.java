public class Main {
    public static void main(String[] args) throws InterruptedException {

        Worker worker = new Worker();
        GoldMine goldMine = new GoldMine(10);
        worker.mine(goldMine);



        while (true) {
            System.out.println("Worker #" + worker.getId() + " has mined " + worker.getGoldAmount() + " gold.");
            Thread.sleep(1000);

            if (goldMine.isEmpty()) {
                return;
            }
        }
    }
}