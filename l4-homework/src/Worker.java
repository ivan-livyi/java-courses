public class Worker extends Thread {

    private static int workersNumber = 0;
    private int id;
    private int goldAmount = 0;

    Worker(){
        id = ++workersNumber;
    }

    @Override
    public void run() {

    }

    public void mine(GoldMine goldMine) throws InterruptedException {
        do {
            goldAmount += goldMine.mine(3);
            Thread.sleep(1000);
        } while (!goldMine.isEmpty());
    }

    public int getGoldAmount() {
        return goldAmount;
    }

    public int getId() {
        return id;
    }
}
