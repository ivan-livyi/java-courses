public class GoldMine {
    private int goldAmount;
    private boolean isEmpty;

    GoldMine(int goldAmount) {
        this.goldAmount = goldAmount;
        isEmpty = goldAmount <= 0;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    int mine(int goldAmount) {
        if (isEmpty) {
            return 0;
        }
        if (this.goldAmount >= goldAmount) {
            this.goldAmount -= goldAmount;
            return goldAmount;
        }
        int mined = this.goldAmount;
        this.goldAmount = 0;
        isEmpty = true;
        return mined;
    }
}
