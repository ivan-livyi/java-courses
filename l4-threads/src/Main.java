import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        /*
        System.out.println(Thread.activeCount());
        System.out.println("start");
        //MyThread myThread = new MyThread();
        MyThread thread1 = new MyThread("A");
        MyThread thread2 = new MyThread("B");
        thread1.start();
        thread2.start();

        while (thread1.isAlive() && thread2.isAlive()) {
            Thread.sleep(1);
        }

        if (thread1.isAlive()) {
            System.out.println(thread2.getThreadName() + " WIN");
        } else {
            System.out.println(thread1.getThreadName() + " WIN");
        }

        System.out.println("end");
        System.out.println(Thread.activeCount());
         */
//        WareHouse wareHouse = new WareHouse();
//        Consumer consumer = new Consumer(wareHouse);
//        Producer producer = new Producer(wareHouse);
 //       Thread.sleep(5000);

        MyThread2 thread = new MyThread2();

            Thread.sleep(3000);
            thread.sle();
            Thread.sleep(3000);
            thread.wak();
            Thread.sleep(3000);
            thread.sle();
            Thread.sleep(3000);
            thread.wak();
    }
}

class MyThread2 extends  Thread {
    private volatile boolean sle = false;

    MyThread2(){
        start();
    }
    public synchronized void sle() throws InterruptedException {
        sle = true;
    }
    public synchronized void wak() {
        notify();
        sle = false;
    }

    @Override
    public  void run() {

            while (true) {
                if (sle) {
                    synchronized (this) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("I'm working");
            }
    }
}

class Consumer extends Thread {
    private WareHouse wareHouse;
    Consumer(WareHouse wareHouse){
        this.wareHouse = wareHouse;
        start();
    }
    @Override
    public void run() {
        while (true) {
            if (wareHouse.isEmpty()) {
                continue;
            }
            System.out.println("Items on storage: " + wareHouse.getStorage());
        }
    }
}
class Producer extends Thread {
    private WareHouse wareHouse;
    Producer(WareHouse wareHouse){
        this.wareHouse = wareHouse;
        start();
    }
    @Override
    public void run() {
        while (true) {
            if (!wareHouse.isEmpty()) {
                continue;
            }
            int random = new Random().nextInt(10);
            wareHouse.putStorage(random);
            System.out.println("Put items on storage: " + random);
        }
    }
}


class WareHouse{
    private int storage = 0;

    volatile boolean empty = true;

    public synchronized boolean isEmpty(){
        return empty;
    }

    public synchronized int getStorage() {
        empty = true;
        return storage;
    }
    public synchronized void putStorage(int value){
        storage = value;
        empty = false;
    }
}
