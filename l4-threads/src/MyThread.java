import java.util.Random;

public class MyThread extends Thread {
    private String name;

    public MyThread (){
        start();
    }

    public String getThreadName(){
        return name;
    }

    public MyThread (String name){
        this.name = name;
    }
    @Override
    public void run() {
        Integer steps = 0;
        do {
            for (int i=0; i< new Random().nextInt(3); i++) {
                steps++;
            }

            System.out.println(name + ": " + "-".repeat(steps) + " (" + steps + ")");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while(steps < 20);
        System.out.println(name + " finished");
    }
}