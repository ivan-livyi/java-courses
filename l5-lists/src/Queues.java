import java.util.ArrayDeque;
import java.util.PriorityQueue;

public class Queues {
    public static void main(String[] args) {
/*
        PriorityQueue<Integer> priority = new PriorityQueue<Integer>();

        priority.add(9);
        priority.add(1);
        priority.add(5);
        priority.add(6);

        //Integer first = priority.peek();
        Integer first = priority.poll();
        //System.out.println(first);
        System.out.println(priority);
*/
        ArrayDeque<Integer> dequeue = new ArrayDeque<Integer>();

        dequeue.offer(9); // to end
        dequeue.offer(1);
        dequeue.add(5);

        dequeue.peek();

        System.out.println(dequeue);
    }
}
