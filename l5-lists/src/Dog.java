public class Dog {
    private int age;
    private String name;
    private boolean hasTail;

    Dog() {

    }

    @Override
    public String toString() {
        return "Name: " + name + ", age: " + age + ", hasTail: " + hasTail;
    }

    public boolean isHasTail() {
        return hasTail;
    }

    public Dog setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
        return this;
    }

    public String getName() {
        return name;
    }

    public Dog setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Dog setAge(int age) {
        this.age = age;
        return this;
    }
}