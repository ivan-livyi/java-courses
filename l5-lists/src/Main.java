import java.util.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // age
        // iterator -> remove without tail

        // array list
        List<Dog> dogList = new ArrayList<Dog>();

        dogList.addAll(Arrays.asList(
                new Dog().setAge(1).setName("Sharik").setHasTail(true),
                new Dog().setAge(8).setName("Bobik").setHasTail(true),
                new Dog().setAge(7).setName("Palkan").setHasTail(false),
                new Dog().setAge(4).setName("Pupsik").setHasTail(true)
        ));
/*
        Iterator<Dog> dogIterator = dogList.iterator();
        while (dogIterator.hasNext()) {
            Dog dog = dogIterator.next();
            if (!dog.isHasTail()) {
                dogIterator.remove();
            }
        }
  */
        for(Dog dog : dogList) {
            if (!dog.isHasTail()) {
                dogList.remove(dog);
            }
        }

        Comparator<Dog> comparator = new Comparator<Dog>() {
            @Override
            public int compare(Dog dog1, Dog dog2) {
                return Integer.compare(dog2.getAge(), dog1.getAge()) * -1;
            }
        };

        dogList.sort(comparator);

        System.out.println(dogList);
    }
}
