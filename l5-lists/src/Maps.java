import java.util.*;

public class Maps {
    public static void main(String[] args) {
        // etag - parkomesto, machina
        // stoyanka - mapa etagey i nomer

        Map<Integer, Car> floor1 = new TreeMap<Integer, Car>();
        floor1.put(1, new Car().setMark("Opel").setYear(2000));
        floor1.put(2, new Car().setMark("Mercedes").setYear(2020));
        floor1.put(3, null);

        Map<Integer, Car> floor2 = new TreeMap<Integer, Car>();
        floor2.put(1, null);
        floor2.put(2, new Car().setMark("Shkoda").setYear(1990));
        floor2.put(3, null);

        Map<Integer, Map<Integer, Car>> parking = new TreeMap<Integer, Map<Integer, Car>>();

        parking.put(1, floor1);
        parking.put(2, floor2);

        for(Integer floorNumber : parking.keySet()){
            System.out.println("Floor: " + floorNumber);
            Map<Integer, Car> floor = parking.get(floorNumber);
            for (Integer place : floor.keySet()) {
                Car car = floor.get(place);
                System.out.println(place + ": " + car);
            }
        }

        for(Map.Entry<Integer, Map<Integer, Car>> entry: parking.entrySet()) {
            System.out.println("Floor: " + entry.getKey());
            for (Map.Entry<Integer, Car> floor : entry.getValue().entrySet()) {
                System.out.println(floor.getKey() + ": " + floor.getValue());
            }
        }
    }

    public static void oldmain(String[] args) {
        //Map<String, Integer> hash = new HashMap<String, Integer>();
//        Map<String, Integer> hash = new LinkedHashMap<String, Integer>(); // preserve order
        Map<String, Integer> hash = new TreeMap<String, Integer>();

        hash.put("one", 1);
        hash.put("two", 2);
        hash.put("three", 3);
        hash.putIfAbsent("four", 4);
        Integer va = hash.getOrDefault("five", 50);

        Set<String> keys = hash.keySet();
        Collection<Integer> values = hash.values();

        for (String key : hash.keySet()) {
            System.out.println(key + " : " + hash.get(key));
        }

        for (Map.Entry<String, Integer> entry : hash.entrySet()) {
            entry.getKey();
            entry.getValue();
        }
    }
}
