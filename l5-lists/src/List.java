import java.util.*;

public class List {
    public static void main(String[] args) {
        LinkedList<String> linked = new LinkedList<>();

        linked.add("head1");
        linked.add("head2");
        linked.add("head3");
        boolean contains = linked.contains("head");

        Iterator<String>  it = linked.iterator();
//
//        it.next();
//        it.hasNext();
//        it.remove();

        for (String element : linked) {
            System.out.println(element);
        }

        ArrayList<String> array = new ArrayList<String>();
        array.add("first");
        array.contains("first");

        array.addAll(Arrays.asList("1", "2", "3"));

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return 0;
            }
        };
        array.sort(comparator);
        System.out.println(array);
    }
}
