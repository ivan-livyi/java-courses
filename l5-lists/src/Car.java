public class Car {
    private int year;
    private String mark;

    @Override
    public String toString() {
        return "[mark: " + mark + ", year: " + year + "]";
    }

    public Car(){

    }

    public int getYear() {
        return year;
    }

    public Car setYear(int year) {
        this.year = year;
        return this;
    }

    public String getMark() {
        return mark;
    }

    public Car setMark(String mark) {
        this.mark = mark;
        return this;
    }
}
