package pa.player;

public class Dog extends Player {

    public Dog(String name) {
        super(name);
        health = 100;
        strikePower = 20;
    }

    protected int calculateStrikePower() {
        // todo: use formula
        return strikePower;
    }
}
