package pa.player;

import pa.FightLog;
import pa.item.Item;

import java.util.ArrayList;
import java.util.List;

abstract public class Player {

    protected String name;

    protected int health;

    protected int strikePower;

    protected List<Item> items = new ArrayList<>();

    Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Player addItem(Item item) {
        items.add(item);
        return this;
    }

    public List<FightLog> fightAgainst(Player opponent) {

        List<FightLog> fightLog = new ArrayList<>();
        fightLog.add(new FightLog(this, opponent));

        while (this.health > 0 && opponent.health > 0) {
            fightLog.add(this.hit(opponent));
            fightLog.add(opponent.hit(this));
        }

        return fightLog;
    }

    protected int calculateStrikePower() {
        return strikePower;
    }

    public int getHealth() {
        return health;
    }

    private FightLog hit(Player opponent) {

        Integer power = calculateStrikePower();
        opponent.health -= power;

        return new FightLog(this).hits(opponent).with(power);
    }
}
