package pa.player;

public class Cat extends Player {

    public Cat(String name) {
        super(name);
        health = 70;
        strikePower = 10;
    }

    protected int calculateStrikePower() {
        // todo: use formula
        return strikePower;
    }
}
