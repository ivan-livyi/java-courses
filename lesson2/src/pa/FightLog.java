package pa;

import pa.player.Player;

public class FightLog {

    private Player attacker;

    private Player defender;

    private String message = "";

    public FightLog(Player attacker, Player defender) {

        message = message.concat("Once upon a time " + attacker.getName() + "[" + attacker.getHealth()
                + "] meet " + defender.getName() + "[" + defender.getHealth() + "] on a highway and a fight begin:\n");
    }

    public FightLog(Player attacker) {
        message = message.concat(" - " + attacker.getName() + "[" + attacker.getHealth() + "] has kicked ");
    }

    public FightLog hits(Player defender) {
        this.defender = defender;
        return this;
    }

    public FightLog with(Integer power) {
        message = message.concat(defender.getName() + "[" + (defender.getHealth() + power) + "]");
        message = message.concat(" with power of " + power + " and now " + defender.getName() + " has " + defender.getHealth() + " health left");
        if (defender.getHealth() <= 0) {
            message = message.concat(". " + defender.getName() + " died.");
        }
        return this;
    }

    @Override
    public String toString() {
        return message;
    }
}
