package pa;

import pa.item.MegaClaw;
import pa.item.SuperPaw;
import pa.player.Cat;
import pa.player.Dog;
import pa.player.Player;

import java.util.List;

class A {
    protected int value = 1;

    public int getValue() {
        return value;
    }
}

class B extends  A {
    {
        value = 2;
    }
    //protected int value = 2;
}


public class Main {

    private static Player cat;
    private static Player dog;

    static {
        cat = new Cat("Murchik").addItem(new SuperPaw()).addItem(new MegaClaw());
        dog = new Dog("Sharik").addItem(new SuperPaw());
    }

    public static void main(String[] args) {

        System.out.println(new B().getValue());
//
//        List<FightLog> fightLog = cat.fightAgainst(dog);
//
//        System.out.println();
//        for (FightLog logItem: fightLog) {
//            System.out.println(logItem);
//        }
//        System.out.println();

    }
}