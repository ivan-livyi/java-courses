package lesson1.fields;

import lesson1.Position;

public class FinishField extends Field {

    public FinishField(Position position) {
        super(position);
    }

    public int getPenalty() {
        return 0;
    }
}
