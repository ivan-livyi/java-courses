package lesson1.fields;

import lesson1.Position;

public class BombField extends Field {
    public BombField(Position position) {
        super(position);
    }

    public int getPenalty() {
        return -20;
    }
}
