package lesson1.fields;

import lesson1.Position;

abstract public class Field {

    private Position position;

    Field(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return this.position;
    }

    abstract public int getPenalty();
}
