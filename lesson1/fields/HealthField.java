package lesson1.fields;

import lesson1.Position;

public class HealthField extends Field {
    public HealthField(Position position) {
        super(position);
    }

    public int getPenalty() {
        return 20;
    }
}
