package lesson1.fields;

import lesson1.Position;

public class RegularField extends Field {

    public RegularField(Position position) {
        super(position);
    }

    public int getPenalty() {
        return -5;
    }
}
