package lesson1;

import lesson1.fields.Field;

public class Player {

    private int health;

    public Position position;

    public Player(int health) {
        this.health = health;
        this.setPosition(new Position(0, 0));
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public boolean inPosition(int x, int y) {
        return this.position.getX() == x && this.position.getY() == y;
    }


    public void moveToField(Field field) {
        this.position = field.getPosition();
        health += field.getPenalty();
    }

    public int getHealth() {
        return this.health;
    }
}
