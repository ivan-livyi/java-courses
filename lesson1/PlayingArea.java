package lesson1;

import lesson1.fields.*;

public class PlayingArea {

    private final int healthPerRow;
    private final int bombPerRow;
    private final int sizeX;
    private final int sizeY;

    private Field[][] area;

    private Player player;

    public PlayingArea(int sizeX, int sizeY, int healthPerRow, int bombPerRow) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.healthPerRow = healthPerRow;
        this.bombPerRow = bombPerRow;
        init(sizeX, sizeY);
    }

    private void init(int sizeX, int sizeY) {
        area = new Field[sizeX][sizeY];

        for (int y = 0; y < sizeY; y++) {
            int minPos = 0;
            int maxPos = sizeX - 1;

            if (y == 0) {
                minPos++;
            } else if (y == sizeY - 1) {
                maxPos--;
            }

            //int[] bombPositions = getRandom(minPos, maxPos, -1, this.bombPerRow);

            int bombPos = getRandom(minPos, maxPos);
            int healthPos = getRandom(minPos, maxPos, bombPos);

            for (int x = 0; x < sizeX; x++) {
                Position position = new Position(x, y);
                if (x == bombPos) {
                    this.area[x][y] = new BombField(position);
                } else if (x == healthPos) {
                    this.area[x][y] = new HealthField(position);
                } else {
                    this.area[x][y] = new RegularField(position);
                }
            }
        }

        this.area[sizeX - 1][sizeY - 1] = new FinishField(new Position(sizeX - 1, sizeY - 1));
    }

    void addPlayer(Player player) {
        this.player = player;
    }

    public Field getField(Position position) {
        // todo: check memory with "byte" instead of "int"
        int x = position.getX();
        int y = position.getY();

        if (x < 0) {
            x = this.sizeX - 1;
        }
        if (x >= this.sizeX) {
            x = 0;
        }
        if (y < 0) {
            y = this.sizeY - 1;
        }
        if (y >= this.sizeY) {
            y = 0;
        }

        Field field = this.area[x][y];

        this.area[x][y] = new RegularField(field.getPosition());

        return field;
    }

    public void render() {
        System.out.println("");


        String topWrapper = "  ";
        for (int x = 0; x < this.sizeX * 3 + 2; x++) {
            topWrapper = topWrapper.concat("=");
        }
        System.out.println(topWrapper);

        for (int y = 0; y < this.area.length; y++) {
            String out = "  |";
            for (int x = 0; x < this.area[y].length; x++) {
                Field field = this.area[x][y];

                if (this.player.inPosition(x, y)) {
                    out = out.concat(" @ ");
                } else if (field instanceof RegularField) {
                    out = out.concat(" . ");
                } else if (field instanceof HealthField) {
                    out = out.concat(" + ");
                } else if (field instanceof BombField) {
                    out = out.concat(" x ");
                } else if (field instanceof FinishField) {
                    out = out.concat(" F ");
                }
            }
            out = out.concat("|");
            System.out.println(out);
        }

        String bottomWrapper = "  ";
        for (int x = 0; x < this.sizeX * 3 + 2; x++) {
            bottomWrapper = bottomWrapper.concat("=");
        }
        System.out.println(bottomWrapper);
        System.out.println("");
    }

    private static int getRandom(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int) (Math.random() * ((max - min) + 1)) + min;
    }

    private static int getRandom(int min, int max, int excluded) {

        int random = getRandom(min, max);

        return random == excluded ? getRandom(min, max, excluded) : random;
    }
/*
    private static int[] getRandom(int min, int max, int excluded, int amount) {
        int[] result;

        for (int i = 0; i < amount; i++) {
            result[i] = getRandom(min, max);
        }
    }*/

}
