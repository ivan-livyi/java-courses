package lesson1;


public class Start {

    public static void main(String[] args) throws Exception {

        PlayingArea area = new PlayingArea(15, 15, 1, 5);
        Player player = new Player(100);
        Game game = new Game(area, player);

        game.start();
        try {
            while (true) {
                game.addMove(System.console().readLine());
                game.render();
            }
        } catch (Exception e) {
            if (e.getMessage().equals("Invalid move")) {
                System.out.println("Invalid character. Allowed: a, s, d, w");
            }
        }
    }
}
