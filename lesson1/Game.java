package lesson1;

import lesson1.fields.Field;
import lesson1.fields.FinishField;

class Game {

    private PlayingArea area;

    private Player player;

    private boolean finished = false;

    private boolean invalidMove = false;

    Game(PlayingArea area, Player player) {
        this.area = area;
        this.player = player;
    }

    public void addMove(String direction) throws Exception {

        Position currentPosition = this.player.getPosition();

        try {
            Position newPosition = new MoveDirection(direction).getNewPosition(currentPosition);
            Field field = area.getField(newPosition);
            if (field instanceof FinishField) {
                this.finished = true;
            }

            this.player.moveToField(field);
        } catch (Exception e) {
            if (e.getMessage().equals("Invalid move")) {
                this.invalidMove = true;
            } else {
                throw e;
            }
        }
    }

    public void start() throws Exception {
        this.area.addPlayer(this.player);
        this.render();
    }

    public void render() throws Exception {
        this.clearScreen();

        System.out.println("");
        System.out.println("  ----------- The Game -----------");
        System.out.println("");
        System.out.println("  Player health: " + player.getHealth());

        this.area.render();

        if (this.player.getHealth() <= 0) {
            System.out.println("  Game over.");
            System.out.println("");
            throw new Exception("game over");
        }
        if (this.finished) {
            System.out.println("  Congratulations! You won!");
            System.out.println("");
            throw new Exception("fin");
        }
        if (this.invalidMove) {
            System.out.println("Invalid character. Allowed: a, s, d, w");
            System.out.println("");
        }
    }

    private void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
