package lesson1;

public class MoveDirection {

    private int direction;

    private final int directionLeft = 0;

    private final int directionRight = 1;

    private final int directionTop = 2;

    private final int directionBottom = 3;

    MoveDirection(String direction) throws Exception {
        switch (direction.toLowerCase()) {
            case "a":
                this.direction = this.directionLeft;
                break;
            case "s":
                this.direction = this.directionBottom;
                break;
            case "d":
                this.direction = this.directionRight;
                break;
            case "w":
                this.direction = this.directionTop;
                break;
            default:
                throw new Exception("Invalid move");
        }
    }

    public Position getNewPosition(Position position) {
        switch (this.direction) {
            case 0: // left
                return new Position(position.getX() - 1, position.getY());
            case 1: // right
                return new Position(position.getX() + 1, position.getY());
            case 2: // top
                return new Position(position.getX(), position.getY() - 1);
            case 3: // bottom
                return new Position(position.getX(), position.getY() + 1);

        }

        return position;
    }
}
